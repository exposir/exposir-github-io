## 基础知识

### **生酮饮食**

**生酮饮食**（英语：ketogenic diet）字面意思是“会产生酮体的饮食”，其为一种高[脂肪](https://zh.wikipedia.org/wiki/%E8%84%82%E8%82%AA)，充足[蛋白质](https://zh.wikipedia.org/wiki/%E8%9B%8B%E7%99%BD%E8%B3%AA)，极[低碳水化合物饮食][https://zh.wikipedia.org/wiki/%e4%bd%8e%e7%a2%b3%e6%b0%b4%e5%8c%96%e5%90%88%e7%89%a9%e9%a3%b2%e9%a3%9f]([1)](<https://zh.wikipedia.org/zh/%E7%94%9F%E9%85%AE%E9%A3%B2%E9%A3%9F#cite_note-Masood2021-1>)。这种饮食的特色是让食物严重缺乏碳水化合物，强迫身体燃烧脂肪而非[碳水化合物](https://zh.wikipedia.org/wiki/%E7%B3%96%E7%B1%BB)，进而产生酮体；医学上主要在用于治疗难以控制（难治）的[儿童癫痫](https://zh.wikipedia.org/w/index.php?title=%E5%85%92%E7%AB%A5%E7%99%B2%E7%99%87&action=edit&redlink=1)。

### **生酮作用**

**生酮作用**（英语：Ketogenesis，又称**酮体生成**）是指[脂肪酸降解](https://zh.wikipedia.org/wiki/%E8%84%82%E8%82%AA%E9%85%B8%E4%BB%A3%E8%AC%9D)过程结果所致的[酮体](https://zh.wikipedia.org/wiki/%E9%85%AE%E4%BD%93)生成过程。

## 酮体

**酮体**（英语：Ketone bodies）是脂肪酸在肝内 β-氧化时特有的中间代谢产物，它包括[丙酮](https://zh.wikipedia.org/wiki/%E4%B8%99%E9%85%AE)、[乙酰乙酸](https://zh.wikipedia.org/wiki/%E4%B9%99%E9%85%B0%E4%B9%99%E9%85%B8)和[β-羟丁酸](https://zh.wikipedia.org/wiki/%CE%92-%E7%BE%A5%E4%B8%81%E9%85%B8)三种化合物。不过严格意义上来讲，β-羟丁酸是一种羟基酸，而非[酮](https://zh.wikipedia.org/wiki/%E9%85%AE)类。当机体处于饥饿、[禁食](https://zh.wikipedia.org/wiki/%E7%A6%81%E9%A3%9F)或某些病理状态（如[糖尿病](https://zh.wikipedia.org/wiki/%E7%B3%96%E5%B0%BF%E7%97%85)），酮体才会大量堆积。酮体可以提供一种替代的能源，对某些组织细胞来说非常重要。

身体在上述状态时，[脂肪](https://zh.wikipedia.org/wiki/%E8%84%82%E8%82%AA)动员加强，大量的[脂肪酸](https://zh.wikipedia.org/wiki/%E8%84%82%E8%82%AA%E9%85%B8)被肝细胞吸收和氧化；而同时为了维持[血糖](https://zh.wikipedia.org/wiki/%E8%A1%80%E7%B3%96)浓度的稳定，也会刺激体内[糖质新生](https://zh.wikipedia.org/wiki/%E7%B3%96%E8%B3%AA%E6%96%B0%E7%94%9F)作用。糖质新生所需原料[草酰乙酸](https://zh.wikipedia.org/wiki/%E8%8D%89%E9%85%B0%E4%B9%99%E9%85%B8)因为被大量消耗，影响到草酰乙酸所参与的另一代谢途径[三羧酸循环](https://zh.wikipedia.org/wiki/%E4%B8%89%E7%BE%A7%E9%85%B8%E5%BE%AA%E7%8E%AF)，使大量中间物[乙酰辅酶 A](https://zh.wikipedia.org/wiki/%E4%B9%99%E9%86%AF%E8%BC%94%E9%85%B6A)无法消耗、出现堆积，因而生成酮体。

## **酮症**

**酮症**是一种代谢状态，当体内的葡萄糖不足时，肝脏会将脂肪转换成脂肪酸与[酮体](https://zh.wikipedia.org/wiki/%E9%85%AE%E4%BD%93)，取代原本由[葡萄糖](https://zh.wikipedia.org/wiki/%E8%91%A1%E8%90%84%E7%B3%96)负责的能量来源。当血中酮体的含量大于 0.5mM，且有长时间的低血糖及低[胰岛素](https://zh.wikipedia.org/wiki/%E8%83%B0%E5%B3%B6%E7%B4%A0)含量，即为‘酮症’。

## **胰岛素抵抗**

胰岛素抵抗（英语：insulin resistance）又称胰岛素抗性，是指胰脏并没有任何病理问题时，脂肪细胞、肌肉细胞和肝细胞对正常浓度的胰岛素反应不足的现象，亦即这些细胞需要更高的胰岛素浓度才能对胰岛素产生反应。随着情况发展，可能胰岛素的分泌量尽管提升很多却也无法满足需求，引起肌肉细胞吸收和肝细胞储备的葡萄糖量降低，以及脂肪细胞储存的甘油三酸酯的水解，分别提升血浆中糖和自由脂肪酸的含量，进而导致代谢综合征。

## 糖异化

糖异生（英语：Gluconeogenesis[1]）又称糖异生作用、糖原发育不良作用，指的是非碳水化合物（乳酸、丙酮酸、甘油、生糖氨基酸等）转变为葡萄糖的过程，所以又称为葡萄糖新生[2]。糖异生保证了机体的血糖水平处于正常水平。糖异生的主要器官是肝。肾在正常情况下糖异生能力只有肝的十分之一，但长期饥饿与酸中毒时肾糖异生能力可大为增强。

## 血脑屏障

**脑血管障壁**（英语：blood–brain barrier ，BBB），也称为**血脑屏障**或**血脑障壁**，指在[血管](https://zh.wikipedia.org/wiki/%E8%A1%80%E7%AE%A1)和[脑](https://zh.wikipedia.org/wiki/%E8%85%A6)之间有一种选择性地阻止某些物质由血液进入大脑的“屏障”。

脑血管障壁几乎不让任何物质通过，除了[氧气](https://zh.wikipedia.org/wiki/%E6%B0%A7%E6%B0%A3)、[二氧化碳](https://zh.wikipedia.org/wiki/%E4%BA%8C%E6%B0%A7%E5%8C%96%E7%A2%B3)和[血糖](https://zh.wikipedia.org/wiki/%E8%A1%80%E7%B3%96)，大部分的[药物](https://zh.wikipedia.org/wiki/%E8%97%A5%E7%89%A9)和[蛋白质](https://zh.wikipedia.org/wiki/%E8%9B%8B%E7%99%BD%E8%B3%AA)由于[分子](https://zh.wikipedia.org/wiki/%E5%88%86%E5%AD%90)结构过大，一般无法通过。

## 大脑供能

葡萄糖作为大脑的主要供能系统，大脑每天能消耗葡萄糖 100 克左右，这些葡萄糖主要由我们的血糖进行供应，所以吃甜的食物会让我们兴奋让大脑兴奋是对的！

如果血糖降低或者是血糖水平降低比如长期饥饿时，大脑无法得到充足的糖分供能就会利用肝生成的酮体提供能量。

饥饿期达到 3 天时每天大脑会消耗肝提供的酮体 50 克左右，饥饿达到两周后每天消耗的酮体可以达到 100 克！但是大脑无法提供正常工作，亢奋会降低！

## Omega-6

欧米茄 6（OMEGA6）在人体内至关重要，[胆固醇](https://baike.baidu.com/item/%E8%83%86%E5%9B%BA%E9%86%87/471445)必须与欧米茄 6 的亚油酸（LA）相结合，才能正常运转和代谢；人脑中的[不饱和脂肪酸](https://baike.baidu.com/item/%E4%B8%8D%E9%A5%B1%E5%92%8C%E8%84%82%E8%82%AA%E9%85%B8/352596)欧米茄 6（OMEGA6）和欧米茄 3（OMEGA3）各占一半；欧米茄 6 的[花生四烯酸](https://baike.baidu.com/item/%E8%8A%B1%E7%94%9F%E5%9B%9B%E7%83%AF%E9%85%B8/1201342)（AA）所产生的[前列腺素](https://baike.baidu.com/item/%E5%89%8D%E5%88%97%E8%85%BA%E7%B4%A0/4802031)PGE2，是人体许多生命功能所必需的激素类化学物质，但它会加速癌细胞的生长，必须由**欧米茄 3**来抑制。

## Omega-3

OMEGA-3 ，又被写作 Ω-3 、ω-3 ，中文称“欧美加 3 ” 、“欧米伽 3 ” ，为一组多元不饱和脂肪酸，常见于深海鱼类、海豹油、和某些植物中，对人体健康十分有益。在化学结构上，OMEGA-3 是一条由碳、氢原子相互连结而成的长链（ 18 个碳原子以上），其间带有 3-6 个不饱和键（即[双键](https://baike.baidu.com/item/%E5%8F%8C%E9%94%AE/4151287)）。因其第一个不饱和键位于甲基一端的第 3 个[碳原子](https://baike.baidu.com/item/%E7%A2%B3%E5%8E%9F%E5%AD%90/8784262)上，故名 OMEGA-3 。

Omega-3[多元不饱和脂肪](https://baike.baidu.com/item/%E5%A4%9A%E5%85%83%E4%B8%8D%E9%A5%B1%E5%92%8C%E8%84%82%E8%82%AA/4265224)酸是人体无法自行合成的[脂肪酸](https://baike.baidu.com/item/%E8%84%82%E8%82%AA%E9%85%B8/478673)，同时也是人体合成各种[荷尔蒙](https://baike.baidu.com/item/%E8%8D%B7%E5%B0%94%E8%92%99/33422)及内生性物质必要的营养素，只有靠食物外来的补充这些油酸，才能让人体的生理机能得以正常运作。

## 酮流感

当你刚开始生酮饮食时，会产生一些类似感冒的症状，比如说头痛，身上发冷，感觉疲劳等等。这种情况，大部分的小酮人在生酮初期都会经历。这种症状被称为"酮流感"(keto flu)，也被称为诱导流感(induction flu)，低碳水化合物流感(low carb flu)或阿特金斯流感(Atkins flu)。

### 症状

1. 脑雾
2. 头疼
3. 发冷
4. 喉咙酸痛
5. 昏昏沉沉
6. 失眠
7. 暴躁
8. 肌肉酸痛
9. 反胃
10. 难以集中注意力
11. 糖瘾
12. 胃痛

### 对策

1. 大量喝水
2. 喝骨头汤
3. 补充电解质
4. 吃更多脂肪，尤其是 MCT 油
5. 保证充足睡眠
6. 做一些温和的锻炼
7. 增加一些碳水摄入

## 多巴胺

多巴胺在整个这个行动发起的过程中，至少起了两个关键作用：

1. 多巴胺设定了门槛的高低。多巴胺越多，发起行动所需要的动力就越低。往往多巴胺越多，人的冲动性行为就越多，多巴胺越低，人就显得越麻木、反应就越慢。以吸毒这个行为为例，多巴胺越多，「摄入毒品」这一举动所需要的动力门槛就越低，大脑就更难抑制住吸毒行为。（帕金森大脑中多巴胺水平偏低）
2. 多巴胺还给行动选择带来了「学习」这个技能。比如说，如果基底核发起了一个行动 A，并且并且行动之后多巴胺水平升高了，中脑皮层通路就会做出相应改变，使得下一次遇到类似的环境/场景时，更倾向于选择行动 A。

## 内啡肽

[跑步者的愉悦感](https://zh.wikipedia.org/w/index.php?title=%E8%B7%91%E6%AD%A5%E8%80%85%E7%9A%84%E6%84%89%E6%82%85%E6%84%9F&action=edit&redlink=1)是指当运动量超过某一阶段时，体内便会分泌脑内啡。长时间、连续、中等至高强度的运动、深呼吸也是分泌脑内啡的条件。长时间运动把肌肉内的[糖原](https://zh.wikipedia.org/wiki/%E7%B3%96%E5%8E%9F)用尽，脑内啡便会分泌。这些运动包括[跑步](https://zh.wikipedia.org/wiki/%E8%B7%91%E6%AD%A5)、[游泳](https://zh.wikipedia.org/wiki/%E6%B8%B8%E6%B3%B3)、[越野滑雪](https://zh.wikipedia.org/wiki/%E8%B6%8A%E9%87%8E%E6%BB%91%E9%9B%AA)、长距离[划船](https://zh.wikipedia.org/wiki/%E5%88%92%E8%88%B9)、骑[单车](https://zh.wikipedia.org/wiki/%E5%96%AE%E8%BB%8A)、[举重](https://zh.wikipedia.org/wiki/%E8%88%89%E9%87%8D)、[有氧运动舞](https://zh.wikipedia.org/wiki/%E6%9C%89%E6%B0%A7%E8%88%9E%E8%B9%88)或球类运动（例如[篮球](https://zh.wikipedia.org/wiki/%E7%B1%83%E7%90%83)、[足球](https://zh.wikipedia.org/wiki/%E8%B6%B3%E7%90%83)或[美式足球](https://zh.wikipedia.org/wiki/%E7%BE%8E%E5%BC%8F%E8%B6%B3%E7%90%83)）。

## 可以吃什么

- 防弹咖啡

防弹咖啡是一种富含脂肪，热量超过 460 卡路里，不含[碳水化合物](https://baike.baidu.com/item/%E7%A2%B3%E6%B0%B4%E5%8C%96%E5%90%88%E7%89%A9/88328)的咖啡。它的配方主要为低霉菌咖啡豆、无盐草饲料黄油、中链甘油三酸酯油（MCT 油，一种易消化脂肪）。

- 黄油
- 椰子油
- 橄榄油
- 防弹咖啡
- 牛排
- 培根
- 黄瓜
- 夏威夷果
- 奶酪

## 不能多吃

## 花生

1. 致癌的黄曲霉素，“偏爱”长在花生上
2. Omega-6 脂肪酸爆高，导致炎症
3. 植酸、凝集素，妨碍营养吸收
4. **非常容易暴食 → 瓜子理论**

> 瓜子理论，[管理学](https://baike.baidu.com/item/%E7%AE%A1%E7%90%86%E5%AD%A6/250)的一门理论。[理论](https://baike.baidu.com/item/%E7%90%86%E8%AE%BA/1732500)内容：1．无论人们喜欢与否，很容易拿起第一颗瓜子；2．一旦吃上第一颗，就会吃起第二颗、第三颗……停不下来；3．在吃瓜子的过程中，人们可能会做一些别的事情，比如去洗手间等等，但回到座位后，都会继续吃瓜子，不[需要](https://baike.baidu.com/item/%E9%9C%80%E8%A6%81)他人提醒、[督促](https://baike.baidu.com/item/%E7%9D%A3%E4%BF%83/2979176)；4．大多数情况下，人们会一直吃下去，直到吃光为止。

# 设备

- 生酮试纸
- 酮体检测仪
