# React 框架选择指北

React 是当前非常流行的用于构建用户界面的 JavaScript 库， 它不仅可以为应用的每一个状态设计出简洁的视图。而且，当数据变动时，React 还能高效更新并渲染合适的组件。

然而要想开发一个完整的前端应用，仅仅使用 React 是远远不够的，我们需要以下各种工具的帮助如：

- 本地环境开发
- 生产环境打包
- opimizations
- 模块按需加载和打包
- 将最新的 ES 语法或 TS 转译成 ES5
- 热更新

手动去实现以上功能是繁琐且没必要的，目前 React 有三个较为流行的框架可供我们选择即：Create React App、Gatsby、Next.js，下面我将逐一对比他们之间的优劣和不同。

![https://user-images.githubusercontent.com/33340988/127197921-1e3393d7-bb63-431e-8060-dc24a1781a0f.jpg](https://user-images.githubusercontent.com/33340988/127197921-1e3393d7-bb63-431e-8060-dc24a1781a0f.jpg)

## **Create React App (CRA)**

Create React App 是 FaceBook 的 React 团队官方出的一个构建 React 单页面应用的脚手架工具。它本身集成了 Webpack，并配置了一系列内置的 loader 和默认的 npm 的脚本，可以很轻松的实现零配置就可以快速开发 React 的应用。

它适用于以下类型的网站：

- 管理后台
- 仪表盘
- 数据分析
- form 表单
- 内网应用

CRA 的优势 ✅ ：

- 官方出品
- 零配置
- CSR（即页面完全在浏览器渲染），简单易于学习
- 服务器和客户的代码完全解耦
- 易于部署，因为打包后的文件是静态文件

CRA 的劣势 ⛔️：

- 打包后的代码可能会臃肿
- 需要手动配置路由、状态管理、代码分割、样式文件等
- 不能用于需要 SEO 检索的网站
- 首屏效果不好，因为 CSR 页面在初始加载时比较慢，

需要注意的是，相比于 Gatsby 和 Next ，CRA 并不是一个框架，正如它官网所描述的：

> Create React App 是一个官方支持的创建 React 单页应用程序的方法。

## **Gatsby**

Gatsby 不仅仅是一个静态网站生成器，它更是一个**渐进式 Web 应用生成器**💪。它的使用背景与 CRA 完全不同。通过 Gatsby 建立的网站，很容易搜索引擎检索到，而且页面的渲染性能非常好。完美支持个人网站、博客、文档网站（PS: React 的官方文档使用的就是 Gatsby），甚至是电子商务网站。而且 Gatsby 可以在构建时通过 GraphQL 获取数据。

可以在 [官方展示页面](https://www.gatsbyjs.com/showcase/) 上查看有哪些页面是用 Gatsby 构建的。

Gatsby 的优势 ✅ ：

- 页面渲染性能优秀
- 对 SEO 友好
- 对打包文件进行了优化
- 轻松部署到 CDN（基于出色的扩展功能）
- 可以创建一个具有离线功能的 PWA 应用
- 丰富的插件系统

Gatsby 的劣势 ⛔️：

- 使用起来相较于 CRA 更为复杂
- 需要了解 GraphQL 和 Node.Js 的相关知识
- 配置繁重
- 构建时间会随着内容的增加而变长
- 有些功能可能需要付费

值得强调的是，丰富的插件系统也是选择 Gatsby 的一个原因，比如 Gatsby 提供许多博客主题插件，其他例如谷歌分析、图片压缩、预加载插件等等。

## **Next.js**

Next.js 适用于高动态或者面向用户的网页，这些页面需要优秀的 SEO，并且可能每分每秒都在变化。

举个例子：今日头条的首页会根据每个人不同的喜好来推送不同的信息流。如果使用 Gatsby 或 Create React App，会首先渲染一个空页面，然后通过 HTTP 调用来获取信息流的新闻数据。然后有了 Next ，可以在服务器端进行数据的获取，并返回完整的页面。

可以在 Next.js 的[展示页面](https://nextjs.org/showcase) 查看有哪些应用是用 Next.js 构建的。

Next.js 的优势 ✅ ：

- 支持服务器端预渲染
- 对 SEO 友好
- 零配置
- 适用于面向用户的高动态内容
- 还可以像 Gatsby 一样做 SSG （ Server Side Generation）

Next.js 的劣势 ⛔️：

- 使用起来比 CRA 更复杂
- SSR 增加了额外的复杂程度
- 扩展取依赖于服务器
- 没有丰富的插件生态系统
- 有些功能可能需要付费

## **总结**

我们需要分析我们想要构建什么类型的网站，以便在 CRA、Gatsby 或 Next.js 之间做出正确的选择，因为他们之间差距很大，适用于不同的场景。

如果我们对项目的需求有足够的了解，在这三者之间挑选就很容易多了。

原文链接：

[When to pick Gatsby, Next.Js or Create React App](https://dev.to/alexandrudanpop/react-applications-when-to-pick-gatsby-or-next-js-or-create-react-app-50l1)
