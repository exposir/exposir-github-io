## Eternity is the most romantic

[Github](https://github.com/exposir) [Notion](https://exposir.notion.site) [Twitter](https://twitter.com/ExposirM)

> 我是一个前端工程师，我用过很多框架和工具写文章、搭博客。我同样看过很多其他人的博客，他们优秀的文章的令人惊胆，有些绝妙的设计的让人过目不忘。可惜的是有些博客已经不再更新，最后一篇文章终止在几年前，最让我遗憾的还是有些博客是已无法打开。所以我选择用 Github 作为我的博客，我希望在可见的未来它可以一直留在互联网上。 Eternity is the most romantic 2022-9-20

- Blog
  - [Vue.js 设计与实现](./docs/Vue.js设计与实现.md)
  - [重学 Javascript](./docs/重学Javascript.md)
  - [《Javascript 悟道》笔记](./docs/《Javascript悟道》笔记.md)
  - [React 框架选择指北](./docs/React框架选择指北.md)
- Life
  - [重庆之旅 Jul 7, 2022](./docs/重庆之旅.md)
  - [生酮饮食（keto）](./docs/生酮饮食keto.md)
  - [徒步游记之香巴拉 May 2, 2022](./docs/徒步游记之香巴拉.md)
  - [《他改变了中国：江泽民传》书摘](./docs/《他改变了中国江泽民传》书摘.md)
- Code
  - [防抖(debounce)、截流(throttle)](./docs/防抖(debounce)截流(throttle).md)
- Else
  - [互联网常用名词](./docs/互联网常用名词.md)
  - [阿波罗计划](./docs/阿波罗计划.md)
